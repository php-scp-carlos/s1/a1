<?php

function getFullAddress($specificAddress, $brgy, $city, $province, $country){
	return "$specificAddress, $brgy, $city, $province, $country";
}

//[SECTION] Selection Control Structures
//Selection control structures are used to make code execution dynamic according to predefined conditions

function getLetterGrade($grade){
	if($grade > 97){
		return "$grade is equivalent to A+";
	}
	else if($grade > 94){
		return "$grade is equivalent to A";
	}
	else if($grade > 91){
		return "$grade is equivalent to A-";
	}
	else if($grade > 88){
		return "$grade is equivalent to B+";
	}
	else if($grade > 85){
		return "$grade is equivalent to B";
	}
	else if($grade > 82){
		return "$grade is equivalent to B-";
	}
	else if($grade > 79){
		return "$grade is equivalent to C+";
	}
	else if($grade > 76){
		return "$grade is equivalent to C";
	}
	else if($grade > 74){
		return "$grade is equivalent to C-";
	}
	else{
		return "$grade is equivalent to D";
	}
}