<?php require_once "./code.php";
//PHP code can be included from another file by using the require_once directive. ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity for S1</title>
</head>
<body>
	<h1>Full Address</h1>
	<p><?= getFullAddress("14 Einstein Street", "San Francisco, Del Monte", "Quezon City", "Metro manila", "Philippines"); ?></p>
	<p><?= getFullAddress("14 Newton Street", "San Jose, Del Monte", "Makati City", "Metro manila", "Philippines"); ?></p>

	<h1>Letter-Based Grading</h1>
	<p><?= getLetterGrade(87); ?></p>
	<p><?= getLetterGrade(94); ?></p>
	<p><?= getLetterGrade(74); ?></p>
</body>
</html>
